/**
 * @typedef {Object} Todo
 * @property {number} id
 * @property {string} text
 * @property {boolean} completed
 */

/** @type {Todo[]} */
let todos = [{ id: Date.now(), text: 'Learn how forms work', completed: false }];

export function getTodos() {
	return todos;
}

/** @param {string} text */
export function addTodo(text) {
	/** @type {Todo} */
	const todo = {
		id: Date.now(),
		text,
		completed: false
	};

	todos.push(todo);
}

/** @param {number} id */
export function removeTodo(id) {
	todos = todos.filter((todo) => todo.id !== id);
}

export function clearTodos() {
	todos = [];
}
