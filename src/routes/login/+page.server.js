/**
 * @typedef LoginForm
 * @property {string} user
 * @property {string} password
 */

import { fail, redirect } from '@sveltejs/kit';
import { zfd } from 'zod-form-data';

/** @type {import('./$types').Actions} */
export const actions = {
	default: async ({ request }) => {
		const formData = await request.formData();

		const loginSchema = zfd.formData({
			user: zfd.text(),
			password: zfd.text()
		});

		const result = loginSchema.safeParse(formData);

		if (!result.success) {
			return fail(400, {
				data: Object.fromEntries(formData),
				/** @type {Record<string, string[]>} */
				errors: result.error.flatten().fieldErrors
			});
		}

		throw redirect(303, '/todos');
	}
};
